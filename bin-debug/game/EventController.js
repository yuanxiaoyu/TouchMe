/**
 * Created by Administrator on 2015-6-20.
 */
var EventController = (function (_super) {
    __extends(EventController, _super);
    function EventController() {
        _super.call(this);
    }
    var d = __define,c=EventController,p=c.prototype;
    EventController.getInstance = function () {
        if (EventController._instance == null)
            EventController._instance = new EventController();
        return EventController._instance;
    };
    EventController.LOW_FRAMERATE = "low_framerate";
    EventController.BLOOD_CHANGED = "blood_changed";
    EventController.ITEM_CHANGED = "item_changed";
    EventController.CANCEL_ITEM_SELECT = "cancel_item_select";
    EventController.ITEM_USE = "item_use";
    EventController.FAIL = "fail";
    return EventController;
})(egret.EventDispatcher);
egret.registerClass(EventController,'EventController');
