/**
 * Created by Administrator on 2015/6/11.
 */
var GameMain = (function (_super) {
    __extends(GameMain, _super);
    function GameMain() {
        _super.call(this);
        this.row = 7;
        this.col = 7;
        this.startX = 40;
        this.startY = 300;
        this.unitMap = {};
        this.score = 0;
        this.result = 0;
        this.curMax = 0;
        this.curSeed = 8;
        this.deleteArr = [];
        this.blood = 5;
        this.gap = 6;
        this.cnt = 0;
        this.fillMap = {};
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.onAddedToStage, this);
    }
    var d = __define,c=GameMain,p=c.prototype;
    p.onAddedToStage = function (event) {
        this.removeEventListener(egret.Event.ADDED_TO_STAGE, this.onAddedToStage, this);
        EventController.getInstance().addEventListener(EventController.FAIL, this.onFail, this);
        if (this._unitCache == null) {
            this._unitCache = [];
            this.StageH = this.stage.stageHeight;
            this.StageW = this.stage.stageWidth;
            this.scoreTf = new egret.TextField();
            this.scoreTf.text = "得分：0";
            this.scoreTf.fontFamily = "微软雅黑";
            this.scoreTf.x = 10;
            this.scoreTf.y = 20;
            this.scoreTf.touchEnabled = false;
            this.addChild(this.scoreTf);
            this.maxNumTf = new egret.TextField();
            this.maxNumTf.text = "最大：0";
            this.maxNumTf.fontFamily = "微软雅黑";
            this.maxNumTf.x = 10;
            this.maxNumTf.y = 70;
            this.maxNumTf.touchEnabled = false;
            this.addChild(this.maxNumTf);
            this.bloodTf = new egret.TextField();
            this.bloodTf.text = "机会：5";
            this.bloodTf.fontFamily = "微软雅黑";
            this.bloodTf.x = 10;
            this.bloodTf.y = 120;
            this.bloodTf.touchEnabled = false;
            this.addChild(this.bloodTf);
            this.initGame();
        }
    };
    p.onFail = function (evt) {
        this.statisticMap = {};
        var value;
        var key;
        var unit;
        for (key in this.unitMap) {
            unit = this.unitMap[key];
            value = unit.getValue();
            if (this.statisticMap[value] == null) {
                this.statisticMap[value] = [];
            }
            this.statisticMap[value].push(key);
        }
        this.doScoreStatistic();
    };
    p.doScoreStatistic = function () {
        var con;
        var len;
        var temparr;
        var temparr1;
        var tw;
        var value;
        var unitWidth = GameUnit.UNIT_WIDTH;
        var flag;
        var unit;
        this.isOver = true;
        //this.score = Model.score;
        var key;
        for (key in this.statisticMap) {
            temparr = this.statisticMap[key];
            len = temparr.length;
            for (var i = 0; i < len; i++) {
                value = parseInt(key) * 10;
                unit = this.unitMap[temparr[i]];
                temparr1 = temparr[i].split("_");
                con = new SomeContainer();
                //game.GetNum(value,con,1,false);
                this.score += value;
                this.scoreTf.text = "得分：" + this.score;
                con.x = (parseInt(temparr1[0]) - 0.3) * unitWidth;
                con.y = parseInt(temparr1[1]) * unitWidth;
                con.alpha = 0;
                tw = egret.Tween.get(con, null, null, true);
                this.effectCon.addChild(con);
                tw.to({ y: con.y - 80, alpha: 1 }, 500).call(con.removeSelf, con);
                ShakeMgr.instance().addShakeItem(unit.name, { gap: 5, cnt: 5, dis: unit, ox: (parseInt(temparr1[0])) * unitWidth, type: "shakeh" });
            }
            flag = true;
            delete this.statisticMap[key];
            break;
        }
        //GameMainPanel.getInstance().startScrollScore();
        if (flag) {
            egret.setTimeout(this.doScoreStatistic, this, 600);
        }
        else {
            egret.setTimeout(this.showGameOver, this, 600);
        }
    };
    p.showGameOver = function () {
        this.effectCon.removeChildren();
        GameConst.GrayMask.visible = true;
    };
    p.initGame = function () {
        this.tree = new DukeTree();
        //this.psystem = new particle.GravityParticleSystem(RES.getRes(this.currentTextureName),RES.getRes("particle_json"));
        this.unitContainer = new egret.DisplayObjectContainer();
        this.unitContainer.x = this.startX;
        this.unitContainer.y = this.startY;
        this.addChild(this.unitContainer);
        this.effectCon = new egret.DisplayObjectContainer();
        this.addChild(this.effectCon);
        this.effectCon.x = this.startX;
        this.effectCon.y = this.startY;
        this.effectCon.touchChildren = this.effectCon.touchEnabled = false;
        this.stage.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTouchUnit, this);
        var unit;
        var gap = GameUnit.UNIT_WIDTH;
        this.getInitValues();
        for (var i = 0; i < this.col; i++) {
            for (var j = 0; j < this.row; j++) {
                unit = this.produceUnit();
                unit.setValue(this.initValues[i * this.col + j]);
                unit.x = i * gap;
                unit.y = j * gap;
                this.unitContainer.addChild(unit);
                unit.name = i + "_" + j;
                this.unitMap[i + "_" + j] = unit;
            }
        }
    };
    p.getInitValues = function () {
        this.initValues = [];
        var value;
        var elematedValues;
        for (var i = 0; i < this.col; i++) {
            for (var j = 0; j < this.row; j++) {
                elematedValues = [];
                value = this.getRandomValue();
                this.result = 0;
                this.checkValue(value, i, j);
                while (this.result >= 2) {
                    elematedValues.push(value);
                    value = this.getRandomValue();
                    while (elematedValues.indexOf(value) != -1) {
                        value = this.getRandomValue();
                    }
                    this.result = 0;
                    this.checkValue(value, i, j);
                }
                this.initValues.push(value);
            }
        }
        this.maxNumTf.text = "最大：" + this.curMax;
    };
    p.checkValue = function (value, col, row, needLoop, rawCol, rawRow) {
        if (needLoop === void 0) { needLoop = true; }
        if (rawCol === void 0) { rawCol = 0; }
        if (rawRow === void 0) { rawRow = 0; }
        var hasNum;
        var index = col * this.col + (row - 1);
        if (index >= 0 && index < this.initValues.length && this.initValues[index] == value && (col != rawCol || row != rawRow)) {
            this.result++;
            if (needLoop) {
                this.checkValue(value, col, row - 1, false, col, row);
            }
        }
        if (this.result >= 2)
            return;
        index = col * this.col + (row + 1);
        if (index >= 0 && index < this.initValues.length && this.initValues[index] == value && (col != rawCol || row != rawRow)) {
            this.result++;
            if (needLoop) {
                this.checkValue(value, col, row + 1, false, col, row);
            }
        }
        if (this.result >= 2)
            return;
        index = (col - 1) * this.col + row;
        if (index >= 0 && index < this.initValues.length && this.initValues[index] == value && (col != rawCol || row != rawRow)) {
            this.result++;
            if (needLoop) {
                this.checkValue(value, col - 1, row, false, col, row);
            }
        }
        if (this.result >= 2)
            return;
        index = (col + 1) * this.col + row;
        if (index >= 0 && index < this.initValues.length && this.initValues[index] == value && (col != rawCol || row != rawRow)) {
            this.result++;
            if (needLoop) {
                this.checkValue(value, col + 1, row, false, col, row);
            }
        }
    };
    p.getRandomValue = function () {
        var seed = this.curSeed < 8 ? 8 : this.curSeed;
        var value = Math.ceil(Math.random() * seed);
        this.curMax = this.curMax > value ? this.curMax : value;
        return value;
    };
    p.setCurMax = function (value) {
        if (value <= this.curMax)
            return;
        this.curMax = value > this.curMax ? value : this.curMax;
        this.curSeed = this.curMax / 2 + 1;
        this.maxNumTf.text = "最大：" + this.curMax;
    };
    p.onTouchUnit = function (event) {
        if (this.inProcessing || this.isOver)
            return;
        if (event.target instanceof GameUnit) {
            this.bonus = 0;
            this.inProcessing = true;
            this.selectedUnit = event.target;
            var node;
            var value = this.selectedUnit.getValue();
            //if( value < 20 )
            //{
            //    SoundMgr.getInstance().playEffect(SoundMgr.EFFECT_BTN);
            value++;
            this.setCurMax(value);
            this.curMax = this.curMax > value ? this.curMax : value;
            node = DukeTree.produce();
            node.data = this.selectedUnit;
            this.tree.reset();
            this.tree.setRoot(node);
            this.selectedUnit.setValue(value);
            this.deleteArr = [];
            this.checkDelete(node);
            var len = this.deleteArr.length;
            if (len >= 2) {
                this.score += len + 1;
                this.scoreTf.text = "得分：" + this.score;
                this.addEventListener(egret.Event.ENTER_FRAME, this.onEnterFrame, this);
                SoundMgr.getInstance().playEffect("1hit_wav");
            }
            else {
                this.inProcessing = false;
                this.blood--;
                if (this.blood <= 0)
                    this.doScoreStatistic();
            }
        }
    };
    p.checkDelete = function (node) {
        var unit = node.data;
        if (unit == null)
            return;
        var tnode;
        var arr = unit.name.split("_");
        var col = parseInt(arr[0]);
        var row = parseInt(arr[1]);
        var value = unit.getValue();
        var unit = this.unitMap[(col + 1) + "_" + row];
        if (unit != null && unit.getValue() == value && this.deleteArr.indexOf(unit) == -1 && unit != this.selectedUnit) {
            tnode = DukeTree.produce();
            tnode.data = unit;
            this.deleteArr.push(unit);
            node.addChild(tnode);
            this.checkDelete(tnode);
        }
        unit = this.unitMap[(col - 1) + "_" + row];
        if (unit != null && unit.getValue() == value && this.deleteArr.indexOf(unit) == -1 && unit != this.selectedUnit) {
            tnode = DukeTree.produce();
            tnode.data = unit;
            this.deleteArr.push(unit);
            node.addChild(tnode);
            this.checkDelete(tnode);
        }
        unit = this.unitMap[col + "_" + (row + 1)];
        if (unit != null && unit.getValue() == value && this.deleteArr.indexOf(unit) == -1 && unit != this.selectedUnit) {
            tnode = DukeTree.produce();
            tnode.data = unit;
            this.deleteArr.push(unit);
            node.addChild(tnode);
            this.checkDelete(tnode);
        }
        unit = this.unitMap[col + "_" + (row - 1)];
        if (unit != null && unit.getValue() == value && this.deleteArr.indexOf(unit) == -1 && unit != this.selectedUnit) {
            tnode = DukeTree.produce();
            tnode.data = unit;
            this.deleteArr.push(unit);
            node.addChild(tnode);
            this.checkDelete(tnode);
        }
    };
    p.updateFillMap = function (hehe) {
        var arr = hehe.split("_");
        if (arr.length == 2) {
            var col = arr[0];
            var row = parseInt(arr[1]);
            var a = this.fillMap[col];
            if (a == null)
                this.fillMap[col] = [row];
            else
                a.push(row);
        }
    };
    p.onEnterFrame = function (event) {
        this.cnt++;
        if (this.cnt == this.gap) {
            this.cnt = 0;
            var leaves = this.tree.getLeaves();
            var len = leaves.length;
            if (len > 0) {
                var node;
                var unit;
                var pnode;
                var punit;
                var tw;
                for (var i = 0; i < len; i++) {
                    node = leaves[i];
                    unit = node.data;
                    pnode = node.getParentNode();
                    punit = pnode.data;
                    tw = egret.Tween.get(unit, null, null, true);
                    tw.to({ x: punit.x, y: punit.y }, 100).call(node.removeSelf, node);
                    pnode.removeChild(node);
                    this.updateScore(unit.getValue() * 10);
                    delete this.unitMap[unit.name];
                    this.updateFillMap(unit.name);
                }
            }
            else {
                this.removeEventListener(egret.Event.ENTER_FRAME, this.onEnterFrame, this);
                if (this.selectedUnit != null) {
                    var value = this.selectedUnit.getValue();
                    this.updateScore(value * 10);
                    this.selectedUnit.setValue(value + 1);
                    this.playHideParticle(this.selectedUnit);
                }
                this.tree.reset();
                egret.setTimeout(this.checkAndFill, this, 200);
            }
        }
    };
    p.updateScore = function (value) {
        this.score += value;
        this.scoreTf.text = "得分：" + this.score;
    };
    //private psystem:particle.GravityParticleSystem;
    p.playHideParticle = function (referUnit) {
        return;
        //var temptextureName:string = "star";
        //this.psystem.stop(true);
        //if( this.currentTextureName != temptextureName)
        //{
        //    this.psystem.changeTexture(RES.getRes(temptextureName));
        //    this.currentTextureName = temptextureName;
        //}
        //
        //this.psystem.emitterX = referUnit.x;
        //this.psystem.emitterY = referUnit.y;
        //this.unitContainer.addChild(this.psystem);
        //this.psystem.start(100);
    };
    p.checkAndFill = function () {
        var needCnt = -1;
        var unit;
        var idx;
        var tw;
        var key;
        for (key in this.fillMap) {
            for (idx = this.row - 1; idx >= 0; idx--) {
                unit = this.unitMap[key + "_" + idx];
                if (unit == null) {
                    needCnt++; //fill empty ones
                }
                else {
                    if (needCnt >= 0) {
                        //unit.alpha = 0;
                        tw = egret.Tween.get(unit, null, null, true);
                        tw.to({ y: unit.y + GameUnit.UNIT_WIDTH * (needCnt + 1) }, 100);
                        delete this.unitMap[unit.name];
                        unit.name = key + "_" + (idx + needCnt + 1);
                        this.unitMap[unit.name] = unit;
                    }
                }
            }
            while (needCnt >= 0) {
                unit = this.produceUnit();
                unit.setValue(this.getRandomValue());
                unit.name = key + "_" + needCnt;
                unit.x = parseInt(key) * GameUnit.UNIT_WIDTH;
                unit.y = -GameUnit.UNIT_WIDTH;
                unit.alpha = 1.0;
                this.unitContainer.addChildAt(unit, 0);
                this.unitMap[unit.name] = unit;
                tw = egret.Tween.get(unit, null, null, true);
                tw.to({ y: unit.y + GameUnit.UNIT_WIDTH * (needCnt + 1) }, 100);
                needCnt--;
            }
            this.maxNumTf.text = "最大：" + this.curMax;
        }
        egret.setTimeout(this.checkAllDeletable, this, 600);
    };
    p.checkAllDeletable = function () {
        var process = false;
        var key;
        var flag = false;
        Outer: for (var i = 0; i < this.row; i++) {
            for (var j = 0; j < this.col; j++) {
                key = j + "_" + i;
                this.tree.reset();
                this.deleteArr = [];
                this.selectedUnit = this.unitMap[key];
                var node = DukeTree.produce();
                node.data = this.unitMap[key];
                this.tree.setRoot(node);
                this.checkDelete(node);
                var len = this.deleteArr.length;
                //console.log("all again..: ",key," value : ",this.unitMap[key].getValue(), " len: ",len);
                if (len >= 2) {
                    this.score += len + 1;
                    this.addEventListener(egret.Event.ENTER_FRAME, this.onEnterFrame, this);
                    process = true;
                    this.bonus++;
                    var hit = this.bonus <= 5 ? this.bonus : 5;
                    SoundMgr.getInstance().playEffect((hit + 1) + "hit_wav");
                    flag = true;
                    break Outer;
                }
            }
        }
        if (!flag) {
            this.scoreTf.text = "得分：" + this.score;
            this.blood += this.bonus;
            this.blood = Math.min(5, this.blood);
            this.bloodTf.text = "血量：" + this.blood;
        }
        this.inProcessing = process;
    };
    p.start = function () {
        if (this.flag) {
            this.addEventListener(egret.Event.ENTER_FRAME, this.onEnterFrame, this);
            this.flag = false;
        }
    };
    p.pause = function () {
        if (this.hasEventListener(egret.Event.ENTER_FRAME)) {
            this.removeEventListener(egret.Event.ENTER_FRAME, this.onEnterFrame, this);
            this.flag = true;
        }
    };
    p.produceUnit = function () {
        if (this._unitCache.length > 0)
            return this._unitCache.pop();
        return new GameUnit();
    };
    p.reclaimUnit = function (unit) {
        if (this._unitCache.indexOf(unit) != -1)
            return;
        this._unitCache.push(unit);
        unit.reset();
    };
    p.reset = function () {
        this.deleteArr = [];
        this.removeEventListener(egret.Event.ENTER_FRAME, this.onEnterFrame, this);
        var unit;
        var key;
        for (key in this.unitMap) {
            unit = this.unitMap[key];
            delete this.unitMap[key];
            this.reclaimUnit(unit);
            this.unitContainer.removeChild(unit);
        }
        var gap = GameUnit.UNIT_WIDTH;
        this.curMax = 0;
        this.curSeed = 5;
        this.blood = 5;
        this.getInitValues();
        for (var i = 0; i < this.col; i++) {
            for (var j = 0; j < this.row; j++) {
                unit = this.produceUnit();
                unit.setValue(this.initValues[i * this.col + j]);
                unit.x = i * gap;
                unit.y = j * gap;
                this.unitContainer.addChild(unit);
                unit.name = i + "_" + j;
                this.unitMap[i + "_" + j] = unit;
            }
        }
        this.flag = false;
        this.isOver = false;
    };
    GameMain.instance = function () {
        if (GameMain._instance == null)
            GameMain._instance = new GameMain();
        return GameMain._instance;
    };
    return GameMain;
})(egret.DisplayObjectContainer);
egret.registerClass(GameMain,'GameMain');
