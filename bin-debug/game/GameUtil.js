/**
 * Created by Administrator on 2015/6/13.
 */
function GetBitmap(texturename) {
    var result = new egret.Bitmap(RES.getRes(texturename));
    result.touchEnabled = false;
    result.cacheAsBitmap = true;
    //result.scaleX = result.scaleY = GameConst.GlobalScale;
    return result;
}
function GetNumString(num, needSplitor) {
    if (needSplitor === void 0) { needSplitor = false; }
    if (!needSplitor || num < 1000)
        return num.toString();
    else {
        var str = num.toString();
        var tstr = "";
        var len = str.length;
        var idx = 0;
        while (len > 0) {
            if (idx != 2 || len == 1) {
                tstr = str.charAt(len - 1) + tstr;
            }
            else {
                tstr = "." + str.charAt(len - 1) + tstr;
                idx = -1;
            }
            idx++;
            len--;
        }
        return tstr;
    }
}
function RemoveAllChild(container) {
    while (container.numChildren != 0) {
        var child = container.removeChildAt(0);
        child = null;
    }
}
