/**
 * Created by Administrator on 2015-6-18.
 */
var RemindPanel = (function (_super) {
    __extends(RemindPanel, _super);
    function RemindPanel() {
        _super.call(this);
    }
    var d = __define,c=RemindPanel,p=c.prototype;
    p.onOk = function (evt) {
        SoundMgr.getInstance().playEffect(SoundMgr.EFFECT_BTN);
        GameConst.GrayMask.visible = false;
        //game.DisplayController.instance().removeUiElement(this);
        if (this.okCallBack != null) {
            this.okCallBack.call(this);
            this.okCallBack = null;
        }
    };
    p.onCancel = function (evt) {
        SoundMgr.getInstance().playEffect(SoundMgr.EFFECT_BTN);
        GameConst.GrayMask.visible = false;
        //game.DisplayController.instance().removeUiElement(this);
        this.curData = null;
        if (this.cancelCallBack != null) {
            this.cancelCallBack.call(this);
            this.cancelCallBack = null;
        }
    };
    //type 0 购买  type 1死亡提醒
    p.showContent = function (value, type) {
        GameConst.GrayMask.visible = true;
        this.curData = value;
    };
    RemindPanel.instance = function () {
        if (RemindPanel._instance == null)
            RemindPanel._instance = new RemindPanel();
        return RemindPanel._instance;
    };
    return RemindPanel;
})(egret.DisplayObjectContainer);
egret.registerClass(RemindPanel,'RemindPanel');
