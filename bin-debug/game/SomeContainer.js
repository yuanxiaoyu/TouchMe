/**
 * Created by Administrator on 2015-7-2.
 */
var SomeContainer = (function (_super) {
    __extends(SomeContainer, _super);
    function SomeContainer() {
        _super.call(this);
    }
    var d = __define,c=SomeContainer,p=c.prototype;
    p.removeSelf = function () {
        if (this.parent != null)
            this.parent.removeChild(this);
        if (this.numChildren)
            this.removeChildren();
        egret.Tween.removeTweens(this);
    };
    return SomeContainer;
})(egret.DisplayObjectContainer);
egret.registerClass(SomeContainer,'SomeContainer');
