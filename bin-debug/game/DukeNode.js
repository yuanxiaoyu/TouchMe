/**
 * Created by Ado on 2015/6/24.
 */
var DukeNode = (function (_super) {
    __extends(DukeNode, _super);
    function DukeNode() {
        _super.call(this);
        this._children = [];
    }
    var d = __define,c=DukeNode,p=c.prototype;
    p.addChild = function (node) {
        if (this._children.indexOf(node) == -1) {
            this._children.push(node);
            node.setParentNode(this);
        }
    };
    p.removeChild = function (node) {
        var idx = this._children.indexOf(node);
        if (idx != -1) {
            this._children.splice(idx, 1);
            node.setParentNode(null);
        }
    };
    p.children = function () {
        return this._children;
    };
    p.isRoot = function () {
        return this._parentNode == null;
    };
    p.getParentNode = function () {
        return this._parentNode;
    };
    p.setParentNode = function (node) {
        this._parentNode = node;
    };
    p.isChildRoot = function () {
        return this._children.length > 1;
    };
    p.isLeaf = function () {
        return this._children.length == 0 && this._parentNode != null;
    };
    p.resetChildren = function () {
        var node;
        while (this._children.length != 0) {
            node = this._children.pop();
            node.resetChildren();
            node.reset();
        }
        DukeTree.reclaimNode(this);
    };
    p.removeSelf = function () {
        if (this._parentNode != null)
            this._parentNode.removeChild(this);
        if (this.data && this.data.parent != null) {
            this.data.parent.removeChild(this.data);
            GameMain.instance().reclaimUnit(this.data);
        }
        DukeTree.reclaimNode(this);
    };
    p.reset = function () {
        this._parentNode = null;
        this._children = [];
        this.data = null;
    };
    return DukeNode;
})(egret.HashObject);
egret.registerClass(DukeNode,'DukeNode');
