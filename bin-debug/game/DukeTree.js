/**
 * Created by Ado on 2015/6/24.
 */
var DukeTree = (function (_super) {
    __extends(DukeTree, _super);
    function DukeTree() {
        _super.call(this);
    }
    var d = __define,c=DukeTree,p=c.prototype;
    p.getRoot = function () {
        return this._root;
    };
    p.setRoot = function (DukeNode) {
        this._root = DukeNode;
    };
    p.getLeaves = function () {
        var arr = [];
        this.getDukeNodeLeaf(this._root, arr);
        return arr;
    };
    p.getDukeNodeLeaf = function (DukeNode, arr) {
        if (DukeNode.isLeaf()) {
            arr.push(DukeNode);
        }
        else {
            var temparr = DukeNode.children();
            var len = temparr.length;
            var DukeNode;
            for (var i = 0; i < len; i++) {
                DukeNode = temparr[i];
                if (DukeNode.isLeaf())
                    arr.push(DukeNode);
                else
                    this.getDukeNodeLeaf(DukeNode, arr);
            }
        }
    };
    p.reset = function () {
        if (this._root != null)
            this._root.reset();
        this._root = null;
    };
    DukeTree.reclaimNode = function (dukeNode) {
        dukeNode.reset();
        if (DukeTree._DukeNodeCache.indexOf(dukeNode) == -1)
            DukeTree._DukeNodeCache.push(dukeNode);
    };
    DukeTree.produce = function () {
        if (DukeTree._DukeNodeCache.length != 0)
            return DukeTree._DukeNodeCache.pop();
        return new DukeNode();
    };
    DukeTree._DukeNodeCache = [];
    return DukeTree;
})(egret.HashObject);
egret.registerClass(DukeTree,'DukeTree');
