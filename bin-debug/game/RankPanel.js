/**
 * Created by Administrator on 2015/6/15.
 */
var RankPanel = (function (_super) {
    __extends(RankPanel, _super);
    function RankPanel() {
        _super.call(this);
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.addEvents, this);
        this.addEventListener(egret.Event.REMOVED_FROM_STAGE, this.removeEvents, this);
    }
    var d = __define,c=RankPanel,p=c.prototype;
    p.addEvents = function (evt) {
    };
    p.removeEvents = function (evt) {
    };
    p.onStart = function (event) {
        SoundMgr.getInstance().playEffect(SoundMgr.EFFECT_BTN);
        //game.DisplayController.instance().removeUiElement(this);
        //game.DisplayController.instance().addGameElement(game.GameMainPanel.getInstance());
        //game.GameMainPanel.getInstance().updateItemNum();
        //game.DisplayController.instance().addGameElement(GameMain.instance());
    };
    RankPanel.getInstance = function () {
        if (RankPanel._instance == null)
            RankPanel._instance = new RankPanel();
        return RankPanel._instance;
    };
    return RankPanel;
})(egret.DisplayObjectContainer);
egret.registerClass(RankPanel,'RankPanel');
