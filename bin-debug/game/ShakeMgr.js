/**
 * Created by DukeCrushIt on 2015/7/7.
 */
var ShakeMgr = (function (_super) {
    __extends(ShakeMgr, _super);
    function ShakeMgr() {
        _super.call(this);
        this.flag = false;
        this.gap = 3;
        this.shakeItems = {};
    }
    var d = __define,c=ShakeMgr,p=c.prototype;
    /**
     * {id:IHashId,dis:DisplayObject,offx:number,offy:number}
     * **/
    p.addShakeItem = function (name, obj) {
        if (this.shakeItems[name] != null) {
            var obj1 = this.shakeItems[name];
            this.removeShakeItem(name);
        }
        obj["gap"] = obj["gap"] != 0 ? obj["gap"] : 5; //distance each shake
        obj["cnt"] = obj["cnt"] != 0 ? obj["cnt"] : 5; //times
        this.shakeItems[name] = obj;
        if (!this.flag) {
            GameConst.Stage.addEventListener(egret.Event.ENTER_FRAME, this.onEnterFrame, this);
            this.flag = true;
        }
    };
    p.onEnterFrame = function (evt) {
        this.gap--;
        var has = false;
        if (this.gap == -1) {
            var obj;
            var dis;
            var shakeType;
            for (var key in this.shakeItems) {
                obj = this.shakeItems[key];
                dis = obj["dis"];
                shakeType = obj["type"];
                if (shakeType == "shakeh") {
                    if (obj["cnt"] == 0) {
                        this.removeShakeItem(key);
                        dis.x = obj["ox"];
                    }
                    else {
                        obj["cnt"]--;
                        if (obj["cnt"] % 2 == 0) {
                            dis.x = obj["ox"] + obj["gap"];
                        }
                        else {
                            dis.x = obj["ox"] - obj["gap"];
                        }
                    }
                }
                else if (shakeType == "shakev") {
                    if (obj["cnt"] == 0) {
                        this.removeShakeItem(key);
                        dis.y = obj["oy"];
                    }
                    else {
                        obj["cnt"]--;
                        if (obj["cnt"] % 2 == 0) {
                            dis.y = obj["oy"] + obj["gap"];
                        }
                        else {
                            dis.y = obj["oy"] - obj["gap"];
                        }
                    }
                }
                else if (shakeType == "twist") {
                    if (obj["cnt"] == 0) {
                        this.removeShakeItem(key);
                        dis.rotation = 0;
                    }
                    else {
                        obj["cnt"]--;
                        if (obj["cnt"] % 2 == 0) {
                            dis.rotation = obj["gap"];
                        }
                        else {
                            dis.rotation = -obj["gap"];
                        }
                    }
                }
                has = true;
            }
            this.gap = 3;
            if (!has) {
                GameConst.Stage.removeEventListener(egret.Event.ENTER_FRAME, this.onEnterFrame, this);
                this.flag = false;
            }
        }
    };
    p.removeShakeItem = function (name) {
        if (this.shakeItems[name] == null)
            return;
        var dis = this.shakeItems[name]["dis"];
        if (this.shakeItems[name].type == "shakeh") {
            dis.x = this.shakeItems[name]["ox"];
        }
        else if (this.shakeItems[name].type == "shakev") {
            dis.y = this.shakeItems[name]["oy"];
        }
        else if (this.shakeItems[name].type == "twist") {
            dis.rotation = 0;
        }
        delete this.shakeItems[name];
    };
    p.pause = function () {
        if (this.flag)
            GameConst.Stage.removeEventListener(egret.Event.ENTER_FRAME, this.onEnterFrame, this);
    };
    p.resume = function () {
        if (this.flag)
            GameConst.Stage.addEventListener(egret.Event.ENTER_FRAME, this.onEnterFrame, this);
    };
    p.reset = function () {
        for (var key in this.shakeItems) {
            this.removeShakeItem(key);
        }
        this.flag = false;
        GameConst.Stage.removeEventListener(egret.Event.ENTER_FRAME, this.onEnterFrame, this);
    };
    ShakeMgr.instance = function () {
        if (ShakeMgr._instance == null)
            ShakeMgr._instance = new ShakeMgr();
        return ShakeMgr._instance;
    };
    return ShakeMgr;
})(egret.HashObject);
egret.registerClass(ShakeMgr,'ShakeMgr');
