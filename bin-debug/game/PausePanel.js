/**
 * Created by Administrator on 2015-6-18.
 */
var PausePanel = (function (_super) {
    __extends(PausePanel, _super);
    function PausePanel() {
        _super.call(this);
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.addEvents, this);
        this.addEventListener(egret.Event.REMOVED_FROM_STAGE, this.removeEvents, this);
    }
    var d = __define,c=PausePanel,p=c.prototype;
    p.addEvents = function () {
        this.startBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onStart, this);
        this.btn_home.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onHome, this);
        this.btn_refresh.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onRefresh, this);
        //RenderRects.getInstance().changeRenderScene(RenderRects.Render_Pause);
    };
    p.removeEvents = function () {
        this.startBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onStart, this);
        this.btn_home.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onHome, this);
        this.btn_refresh.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onRefresh, this);
    };
    p.onStart = function (evt) {
        SoundMgr.getInstance().playEffect(SoundMgr.EFFECT_BTN);
        GameConst.GrayMask.visible = false;
        GameMain.instance().start();
        ShakeMgr.instance().resume();
    };
    p.onHome = function (evet) {
        SoundMgr.getInstance().playEffect(SoundMgr.EFFECT_BTN);
        GameConst.GrayMask.visible = false;
        GameMain.instance().reset();
        ShakeMgr.instance().reset();
    };
    p.onRefresh = function (evt) {
        SoundMgr.getInstance().playEffect(SoundMgr.EFFECT_BTN);
        GameConst.GrayMask.visible = false;
        GameMain.instance().reset();
        ShakeMgr.instance().reset();
    };
    PausePanel.instance = function () {
        if (PausePanel._instance == null)
            PausePanel._instance = new PausePanel();
        return PausePanel._instance;
    };
    return PausePanel;
})(egret.DisplayObjectContainer);
egret.registerClass(PausePanel,'PausePanel');
