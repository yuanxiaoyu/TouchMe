/**
 * Created by Administrator on 2015-7-2.
 */
var SoundMgr = (function (_super) {
    __extends(SoundMgr, _super);
    function SoundMgr() {
        _super.call(this);
        this.isSoundLoaded = false;
        this.channelMap = {}; //channel hashcode-sound path
        this.playingEffect = {};
    }
    var d = __define,c=SoundMgr,p=c.prototype;
    p.playBg = function (hehe) {
        if (hehe === void 0) { hehe = null; }
        return;
        if (this.bgChannel != null) {
            this.bgChannel.stop();
        }
        if (RES.hasRes(SoundMgr.BATTLE_BG))
            RES.destroyRes(SoundMgr.BATTLE_BG);
        var sound = RES.getRes(SoundMgr.BG);
        sound.type = egret.Sound.MUSIC;
        this.bgChannel = sound.play();
    };
    p.playEffect = function (str, name) {
        if (name === void 0) { name = ""; }
        return;
        if (this.playingEffect[str])
            return;
        var effectSound = RES.getRes(str);
        effectSound.type = egret.Sound.EFFECT;
        this.playingEffect[str] = egret.getTimer();
        var channel = effectSound.play(0, 1);
        this.channelMap[channel.hashCode] = str;
        channel.addEventListener(egret.Event.SOUND_COMPLETE, this.effectComplete, this);
    };
    p.effectComplete = function (evt) {
        var channel = evt.target;
        channel.removeEventListener(egret.Event.SOUND_COMPLETE, this.effectComplete, this);
        var name = this.channelMap[channel.hashCode];
        delete this.playingEffect[name];
    };
    p.checkUnfinishedEffects = function () {
        var checkTime = egret.getTimer();
        var time;
        for (var key in this.playingEffect) {
            time = this.playingEffect[key];
            if (checkTime - time > 5000) {
                delete this.playEffect(key);
            }
        }
    };
    SoundMgr.getInstance = function () {
        if (SoundMgr._instance == null)
            SoundMgr._instance = new SoundMgr();
        return SoundMgr._instance;
    };
    SoundMgr.BG = "bgmsc_mp3";
    SoundMgr.TOUCHMOVE = "touchmove_wav";
    SoundMgr.GATHER = "gather_wav";
    SoundMgr.HAIR = "hair_wav";
    SoundMgr.MIOW_1 = "miow_1_wav";
    SoundMgr.UPGRADE = "upgrade_wav";
    SoundMgr.USEITEM = "useitem_wav";
    SoundMgr.BARK_1 = "bark_1_wav";
    SoundMgr.EFFECT_BTN = "btn_wav";
    SoundMgr.CHOSE = "chosepet_wav";
    SoundMgr.BATTLE_BG = "battle_bg_mp3";
    return SoundMgr;
})(egret.EventDispatcher);
egret.registerClass(SoundMgr,'SoundMgr');
