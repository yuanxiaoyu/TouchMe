/**
 * Created by Administrator on 2015/6/11.
 */
var GameUnit = (function (_super) {
    __extends(GameUnit, _super);
    function GameUnit() {
        _super.call(this);
        this.value = 0;
        this.init();
        this.touchEnabled = true;
    }
    var d = __define,c=GameUnit,p=c.prototype;
    p.init = function () {
        this.bottom = new egret.Bitmap(RES.getRes("botton_png"));
        this.bottom.x = -this.bottom.texture.textureWidth >> 1;
        this.bottom.y = -this.bottom.texture.textureHeight >> 1;
        this.addChild(this.bottom);
        this.numTxt = new egret.TextField();
        this.numTxt.touchEnabled = false;
        this.numTxt.fontFamily = "Arial";
        this.numTxt.textColor = 0;
        this.addChild(this.numTxt);
    };
    p.setValue = function (value) {
        if (this.value == value)
            return;
        this.value = Math.floor(value);
        this.numTxt.text = this.value + "";
        this.numTxt.x = -this.numTxt.textWidth >> 1;
        this.numTxt.y = -this.numTxt.textHeight >> 1;
    };
    p.getValue = function () {
        return this.value;
    };
    p.removeSelf = function () {
        if (this.parent != null) {
            this.parent.removeChild(this);
        }
        GameMain.instance().reclaimUnit(this);
    };
    p.reset = function () {
        this.name = "";
    };
    GameUnit.UNIT_WIDTH = 64;
    return GameUnit;
})(egret.DisplayObjectContainer);
egret.registerClass(GameUnit,'GameUnit');
