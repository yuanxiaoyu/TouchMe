/**
 * Created by Administrator on 2015/6/13.
 */
function GetBitmap(texturename:string):egret.Bitmap{
    var result:egret.Bitmap = new egret.Bitmap(RES.getRes(texturename));
    result.touchEnabled = false;
    result.cacheAsBitmap = true;
    //result.scaleX = result.scaleY = GameConst.GlobalScale;
    return result;
}

function GetNumString(num:number,needSplitor:boolean=false):string{
    if( !needSplitor || num < 1000)
        return num.toString();
    else{
        var str:string = num.toString();
        var tstr:string = "";
        var len:number = str.length;
        var idx:number = 0;
        while(len > 0){
            if( idx != 2 || len == 1){
                tstr = str.charAt(len-1)+tstr;
            }else{
                tstr = "."+str.charAt(len-1)+tstr;
                idx = -1;
            }
            idx++;
            len--;
        }
        return tstr;
    }
}


function RemoveAllChild(container:egret.DisplayObjectContainer){
    while(container.numChildren != 0){
        var child:any = container.removeChildAt(0);
        child = null;
    }
}