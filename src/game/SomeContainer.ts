/**
 * Created by Administrator on 2015-7-2.
 */
class SomeContainer extends egret.DisplayObjectContainer{
    public constructor(){
        super();
    }

    public removeSelf(){
        if( this.parent != null)
            this.parent.removeChild(this);
        if( this.numChildren)
            this.removeChildren();
        egret.Tween.removeTweens(this);
    }
}