/**
 * Created by Administrator on 2015-6-20.
 */
class EventController extends egret.EventDispatcher{
    public static LOW_FRAMERATE:string = "low_framerate";
    public static BLOOD_CHANGED:string = "blood_changed";
    public static ITEM_CHANGED:string = "item_changed";
    public static CANCEL_ITEM_SELECT:string = "cancel_item_select";
    public static ITEM_USE:string = "item_use";
    public static FAIL:string = "fail";
    public constructor(){
        super();
    }

    private static _instance:EventController;
    public static getInstance():EventController{
        if(EventController._instance == null)
            EventController._instance = new EventController();

        return EventController._instance;
    }
}