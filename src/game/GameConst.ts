/**
 * Created by Administrator on 2015/6/15.
 */
class GameConst{
    public static StageW:number;
    public static StageH:number;
    public static GlobalScale:number;
    public static GrayMask:egret.Shape;
    public static Stage:egret.Stage;
    public static ParticlTetureConfig:number[] = [1,2,3,4,5,6,2,8,5,3,1,2,3,4,5,6,2,8,5,3] ;
    public static lowModel:boolean = false;
    public static Res_Root:string;
    public static Version:string;
    public static Api_Url:string;
}