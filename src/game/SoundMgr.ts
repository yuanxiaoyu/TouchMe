/**
 * Created by Administrator on 2015-7-2.
 */
class SoundMgr extends egret.EventDispatcher{
    public static BG:string = "bgmsc_mp3";
    public static TOUCHMOVE:string = "touchmove_wav";
    public static GATHER:string = "gather_wav";
    public static HAIR:string = "hair_wav";
    public static MIOW_1:string = "miow_1_wav";
    public static UPGRADE:string = "upgrade_wav";
    public static USEITEM:string = "useitem_wav";
    public static BARK_1:string = "bark_1_wav";
    public static EFFECT_BTN:string = "btn_wav";
    public static CHOSE:string = "chosepet_wav";
    public static BATTLE_BG:string = "battle_bg_mp3";
    private playingEffect:Object;
    public isSoundLoaded:boolean = false;
    public constructor(){
        super();
        this.playingEffect = {};
    }

    private bgChannel:egret.SoundChannel;
    public playBg(hehe:any = null){
        return;
        if( this.bgChannel != null){
            this.bgChannel.stop();
        }
        if( RES.hasRes(SoundMgr.BATTLE_BG))
            RES.destroyRes(SoundMgr.BATTLE_BG);
        var sound:egret.Sound= RES.getRes(SoundMgr.BG);
        sound.type = egret.Sound.MUSIC;
        this.bgChannel = sound.play();
    }

    private channelMap={};//channel hashcode-sound path
    public playEffect(str:any,name:string=""){
        return;
        if( this.playingEffect[str]) return;

        var effectSound:egret.Sound= RES.getRes(str);
        effectSound.type = egret.Sound.EFFECT;
        this.playingEffect[str] = egret.getTimer();

        var channel:egret.SoundChannel = effectSound.play(0,1);
        this.channelMap[channel.hashCode] = str;
        channel.addEventListener(egret.Event.SOUND_COMPLETE, this.effectComplete, this);
    }

    private effectComplete(evt:egret.Event){
        var channel = evt.target;
        channel.removeEventListener(egret.Event.SOUND_COMPLETE, this.effectComplete, this);
        var name = this.channelMap[channel.hashCode];
        delete this.playingEffect[name];
    }

    public checkUnfinishedEffects(){
        var checkTime = egret.getTimer();
        var time:number;
        for(var key in this.playingEffect){
            time = this.playingEffect[key];
            if( checkTime - time > 5000){
                delete  this.playEffect(key);
            }
        }
    }

    private static _instance:SoundMgr;
    public static getInstance():SoundMgr{
        if( SoundMgr._instance == null)
            SoundMgr._instance = new SoundMgr();

        return SoundMgr._instance;
    }
}