/**
 * Created by Administrator on 2015-6-18.
 */
class RemindPanel extends egret.DisplayObjectContainer{
    public btn_cancel:egret.Bitmap;
    public btn_ok:egret.Bitmap;
    public lab_remind:egret.Bitmap;
    public lab_cancel:egret.Bitmap;
    public lab_confirm:egret.Bitmap;
    public curData:Object;
    public constructor(){
        super();
    }


    public onOk(evt:egret.TouchEvent){
        SoundMgr.getInstance().playEffect(SoundMgr.EFFECT_BTN);
        GameConst.GrayMask.visible = false;
        //game.DisplayController.instance().removeUiElement(this);
        if( this.okCallBack != null){
            this.okCallBack.call(this);
            this.okCallBack = null;
        }
    }
    private okCallBack:Function;
    private cancelCallBack:Function;

    public onCancel(evt:egret.TouchEvent){
        SoundMgr.getInstance().playEffect(SoundMgr.EFFECT_BTN);
        GameConst.GrayMask.visible = false;
        //game.DisplayController.instance().removeUiElement(this);
        this.curData = null;
        if( this.cancelCallBack != null){
            this.cancelCallBack.call(this);
            this.cancelCallBack = null;
        }
    }

    //type 0 购买  type 1死亡提醒
    public showContent(value:Object,type:number){
        GameConst.GrayMask.visible = true;
        this.curData = value;
    }
    private static _instance:RemindPanel;
    public static instance():RemindPanel{
        if( RemindPanel._instance == null)
            RemindPanel._instance = new RemindPanel();

        return RemindPanel._instance;
    }
}