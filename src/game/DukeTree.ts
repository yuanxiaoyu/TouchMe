/**
 * Created by Ado on 2015/6/24.
 */
class DukeTree extends egret.HashObject{
    private _root:DukeNode;
    public constructor(){
        super();
    }

    public getRoot():DukeNode{
        return this._root;
    }

    public setRoot(DukeNode:DukeNode){
        this._root = DukeNode;
    }

    public getLeaves():DukeNode[]{
        var arr:DukeNode[] = [];
        this.getDukeNodeLeaf(this._root, arr);
        return arr;
    }

    private  getDukeNodeLeaf(DukeNode:DukeNode,arr:DukeNode[]){
        if( DukeNode.isLeaf()){
            arr.push(DukeNode);
        }else{
            var temparr:DukeNode[] = DukeNode.children();
            var len:number = temparr.length;
            var DukeNode:DukeNode;
            for(var i:number = 0; i< len; i++){
                DukeNode = temparr[i];
                if( DukeNode.isLeaf() )
                    arr.push(DukeNode);
                else
                    this.getDukeNodeLeaf(DukeNode, arr);
            }
        }
    }
    public reset(){
        if( this._root != null)
            this._root.reset();
        this._root = null;
    }
    private static _DukeNodeCache:DukeNode[] = [];
    public static reclaimNode(dukeNode:DukeNode){
        dukeNode.reset();
        if( DukeTree._DukeNodeCache.indexOf(dukeNode)==-1)
            DukeTree._DukeNodeCache.push(dukeNode);
    }

    public static produce():DukeNode{
        if(DukeTree._DukeNodeCache.length != 0)
            return DukeTree._DukeNodeCache.pop();
        return new DukeNode();
    }
}