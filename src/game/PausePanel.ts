/**
 * Created by Administrator on 2015-6-18.
 */
class PausePanel extends egret.DisplayObjectContainer{
    public startBtn:egret.Bitmap;
    public btn_home:egret.Bitmap;
    public btn_refresh:egret.Bitmap;
    public constructor(){
        super();
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.addEvents, this);
        this.addEventListener(egret.Event.REMOVED_FROM_STAGE, this.removeEvents, this);
    }

    private addEvents(){
        this.startBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onStart, this);
        this.btn_home.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onHome, this);
        this.btn_refresh.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onRefresh, this);
        //RenderRects.getInstance().changeRenderScene(RenderRects.Render_Pause);
    }

    private removeEvents(){
        this.startBtn.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onStart, this);
        this.btn_home.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onHome, this);
        this.btn_refresh.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onRefresh, this);
    }

    private onStart(evt:egret.TouchEvent)
    {
        SoundMgr.getInstance().playEffect(SoundMgr.EFFECT_BTN);
        GameConst.GrayMask.visible = false;
        GameMain.instance().start();
        ShakeMgr.instance().resume();
    }

    private onHome(evet:egret.TouchEvent)
    {
        SoundMgr.getInstance().playEffect(SoundMgr.EFFECT_BTN);
        GameConst.GrayMask.visible = false;
        GameMain.instance().reset();
        ShakeMgr.instance().reset();
    }

    private onRefresh(evt:egret.TouchEvent)
    {
        SoundMgr.getInstance().playEffect(SoundMgr.EFFECT_BTN);
        GameConst.GrayMask.visible = false;
        GameMain.instance().reset();
        ShakeMgr.instance().reset();
    }

    private static _instance:PausePanel;
    public static instance():PausePanel{
        if( PausePanel._instance == null)
            PausePanel._instance = new PausePanel();

        return PausePanel._instance;
    }
}