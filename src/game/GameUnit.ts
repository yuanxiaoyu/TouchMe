/**
 * Created by Administrator on 2015/6/11.
 */
class GameUnit extends egret.DisplayObjectContainer{
    private value:number = 0;
    private bottom:egret.Bitmap;
    private numTxt:egret.TextField;
    public static UNIT_WIDTH:number = 64;
    public constructor(){
        super();
        this.init();
        this.touchEnabled = true;
    }

    public init(){
        this.bottom = new egret.Bitmap(RES.getRes("botton_png"));
        this.bottom.x = -this.bottom.texture.textureWidth >> 1;
        this.bottom.y = -this.bottom.texture.textureHeight >> 1;
        this.addChild(this.bottom);
        this.numTxt = new egret.TextField();
        this.numTxt.touchEnabled = false;
        this.numTxt.fontFamily = "Arial";
        this.numTxt.textColor = 0;
        this.addChild(this.numTxt);
    }

    public setValue(value:number){
        if( this.value == value )
            return;
        this.value = Math.floor(value);
        this.numTxt.text = this.value+"";
        this.numTxt.x = -this.numTxt.textWidth >> 1;
        this.numTxt.y = -this.numTxt.textHeight >> 1;
    }
    public getValue():number{
        return this.value;
    }
    public removeSelf(){
        if( this.parent != null)
        {
            this.parent.removeChild(this);
        }
        GameMain.instance().reclaimUnit(this);
    }
    public reset(){
        this.name = "";
    }

}