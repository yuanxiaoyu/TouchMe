/**
 * Created by Administrator on 2015/6/15.
 */
class RankPanel extends egret.DisplayObjectContainer{
    public constructor(){
        super();
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.addEvents, this);
        this.addEventListener(egret.Event.REMOVED_FROM_STAGE, this.removeEvents, this);
    }
    private addEvents(evt:egret.Event){
    }

    private removeEvents(evt:egret.Event){
    }

    public btn_prize:egret.Bitmap;
    public btn_start:egret.Bitmap;

    private onStart(event:egret.TouchEvent)
    {
        SoundMgr.getInstance().playEffect(SoundMgr.EFFECT_BTN);
        //game.DisplayController.instance().removeUiElement(this);
        //game.DisplayController.instance().addGameElement(game.GameMainPanel.getInstance());
        //game.GameMainPanel.getInstance().updateItemNum();
        //game.DisplayController.instance().addGameElement(GameMain.instance());
    }

    private static _instance:RankPanel;
    public static getInstance():RankPanel{
        if( RankPanel._instance == null)
            RankPanel._instance = new RankPanel();

        return RankPanel._instance;
    }
}